package bibliotheque;

import java.util.ArrayList;

public class ListePersonne {
    private ArrayList<Personne> liste;

    public ListePersonne(ArrayList<Personne> liste) {
        this.liste = liste;
    }

    public void ajouterPersonne(Personne p) {
        liste.add(p);
    }

    public boolean appartient(Personne p) {
        return liste.contains(p);
    }

    public boolean appartientNomPrenom(String nom, String prenom) {
        boolean bool = false;
        for (Personne p : liste) {
            if (p.getNom().equals(nom) && p.getPrenom().equals(prenom)) {
                bool = true;
            }
        }
        return bool;
    }

    public ArrayList<Personne> getListe() {
        return liste;
    }
}
