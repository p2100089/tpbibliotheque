package bibliotheque;

import org.junit.Ignore;

public class Personne {
    private int numero;
    private String nom;
    private String prenom;
    private int anNaissance;
    public static int dernierNumero;


    public Personne(String nomPersonne, String prenomPersonne, int anNaissance) {
        this.numero = dernierNumero;
        this.nom = nomPersonne;
        this.prenom = prenomPersonne;
        this.anNaissance = anNaissance;
        this.dernierNumero ++;
    }

    public Personne(String nomPersonne, String prenomPersonne) {
        this.numero = dernierNumero;
        this.nom = nomPersonne;
        this.prenom = prenomPersonne;
        this.dernierNumero ++;

    }

    public Personne(String nomPersonne) {
        this.numero = dernierNumero;
        this.nom = nomPersonne;
        this.dernierNumero ++;

    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the anNaissance
     */
    public int getAnNaissance() {
        return this.anNaissance;
    }

    /**
     * @param anNaissance the anNaissance to set
     */
    public void setAnNaissance(int anNaissance) {
        this.anNaissance = anNaissance;
    }

    /**
     * @return the dernierNumero
     */
    public int getDernierNumero() {
        return this.dernierNumero;
    }

    /**
     * @param dernierNumero the dernierNumero to set
     */
    public void setDernierNumero(int dernierNumero) {
        this.dernierNumero = dernierNumero;
    }

    /**
     * Méthode toString
     */
    @Override
    public String toString() {
        return numero+", "+nom+", "+prenom+", "+anNaissance;
    }
}
