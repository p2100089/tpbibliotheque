package test;

import bibliotheque.ListePersonne;
import bibliotheque.Personne;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ListePersonneTest {
    public ListePersonneTest() {
    }
    @BeforeClass
    public static void setUpClass() {
    }
    @AfterClass
    public static void tearDownClass() {
    }
    @Before
    public void setUp() {
    }
    @After
    public void tearDown() {
    }


    /**
     * Test of get liste method, of class ListePersonne.
     */
    @Test
    public void testGetListe() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        ListePersonne instance = new ListePersonne(liste);
        ArrayList<Personne> expResult = liste;
        ArrayList<Personne> result = instance.getListe();
        assertEquals(expResult, result);
    }

    /**
     * Test of set liste method, of class ListePersonne.
     */
    @Test
    public void testSetListe() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        ListePersonne instance = new ListePersonne(liste);
        ArrayList<Personne> expResult = liste;
        ArrayList<Personne> result = instance.getListe();
        assertEquals(expResult, result);
    }

    /**
     * Test of ajouter method, of class ListePersonne.
     */
    @Test
    public void testAjouter() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        ListePersonne instance = new ListePersonne(liste);
        Personne p = new Personne("Turing", "Alan", 1912);
        instance.ajouterPersonne(p);
        assertEquals(p, instance.getListe().get(0));
    }

    /**
     * Test of appartient method, of class ListePersonne.
     */
    @Test
    public void testAppartient() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        ListePersonne instance = new ListePersonne(liste);
        Personne p = new Personne("Turing", "Alan", 1912);
        instance.ajouterPersonne(p);
        assertEquals(true, instance.appartient(p));
    }

    /**
     * Test of appartientNomPrenom method, of class ListePersonne.
     */
    @Test
    public void testAppartientNomPrenom() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        ListePersonne instance = new ListePersonne(liste);
        Personne p = new Personne("Turing", "Alan", 1912);
        instance.ajouterPersonne(p);
        assertEquals(true, instance.appartientNomPrenom("Turing", "Alan"));
    }
}
