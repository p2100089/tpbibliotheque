package test;

import bibliotheque.Personne;
import org.junit.*;

import static org.junit.Assert.*;

/**
 * @author Amélie Cordier
 */
public class PersonneTest {
    public PersonneTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    /**
     * Test of getNumero method, of class Personne.
     */
    @Test
    @Ignore
    public void testGetNumero() {
        fail("Test à implémenter plus tard");
    }

    /**
     * Test of getNom method, of class Personne.
     */
    @Test
    public void testGetNom() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        assertEquals("Turing", alan.getNom());
    }

    /**
     * Test of getPrenom method, of class Personne.
     */
    @Test
    public void testGetPrenom() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        assertEquals("Alan", alan.getPrenom());
    }

    /**
     * Test of getAnNaissance method, of class Personne.
     */
    @Test
    public void testGetAnNaissance() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        assertEquals(1912, alan.getAnNaissance());
    }

    /**
     * Test of getDernierNum method, of class Personne.
     */
    @Test
    @Ignore
    public void testGetDernierNum() {
        fail("Test à implémenter plus tard");
    }

    /**
     * Test of setNumPers method, of class Personne.
     */
    @Test
    public void testSetNumPers() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        alan.setNumero(18);
        assertEquals(18, alan.getNumero());
    }

    /**
     * Test of setNomPers method, of class Personne.
     */
    @Test
    public void testSetNomPers() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        alan.setNom("Minsky");
        assertEquals("Minsky", alan.getNom());
    }

    /**
     * Test of setPrenomPerso method, of class Personne.
     */
    @Test
    public void testSetPrenomPers() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        alan.setPrenom("Marvin");
        assertEquals("Marvin", alan.getPrenom());
    }

    /**
     * Test of setAnNaissance method, of class Personne.
     */
    @Test
    public void testSetAnNaissance() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        alan.setAnNaissance(1990);
        assertEquals(1990, alan.getAnNaissance());
    }

    /**
     * Test of toString method, of class Personne.
     */
    @Test
    public void testToString() {
        Personne alan = new Personne("Turing", "Alan", 1912);
        assertEquals("0, Turing, Alan, 1912", alan.toString());
    }

    @Test
    public void testImplementationOfId(){
        Personne alan = new Personne("Turing", "Alan", 1912);
        Personne alan2 = new Personne("Turing", "Alan", 1912);
        assertEquals(alan.getNumero()+1, alan2.getNumero());
    }
}

